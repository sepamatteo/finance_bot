# finance_bot
# Progetto di PCTO  sviulppato dalla classe 4J



**Linguaggi utilizzati**: 
1. Python
2. Java

**Moduli da installare**(_python_):
- Requests: `pip3 install requests`

**API utilizzate**:
- [YH Finance](https://rapidapi.com/apidojo/api/yh-finance/)

**Fonte dati**: 
- [Yahoo Finance](https://finance.yahoo.com/)

<pre>

+---------------------------------------------------------DESCRIZIONE--------------------------------------------------------------+
|                                                                                                                                  |
|  Il programma di backend scritto in python esegue il parsing del file ".json" di un titolo e intabella i dati in un file ".csv". |
|                                                                                                                                  |
|  Un programma in java legge i dati dal file ".csv" e li invia al bot.                                                            |
|                                                                                                                                  | 
|  Il Bot in base alla selezione dell'utente restituisce i dati desiderati.                                                        |
|                                                                                                                                  |
+----------------------------------------------------------------------------------------------------------------------------------+

</pre>
